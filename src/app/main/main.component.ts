import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../app.service';
import { climate } from "../data-models/weather";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.less']
})
export class MainComponent implements OnInit {

  climates: climate[] = [];

  constructor(private appService: AppService, private route: Router) {}
  
  ngOnInit() {
    this.appService.getCities().subscribe((response: any) => {
      this.climates = response;
    });
  }

  getForecast(city: string) {
    localStorage.setItem("city", city);
    this.route.navigate(['details']);
  }
}

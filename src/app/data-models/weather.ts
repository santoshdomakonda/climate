export interface climate {
    base: string;
    clouds: cloud;
    cod: number;
    coord: coordinates;
    dt: number;
    id: number;
    main: main;
    name: string;
    sys: sys;
    timezone: number;
    visibility: number;
    weather: weather[];
    wind: wind;
}

export interface cloud{
    all: number;
}

export interface coordinates {
    lat: number;
    lon: number;
}

export interface main {
    feels_like: number;
    humidity: number;
    pressure: number;
    temp: number;
    temp_max: number;
    temp_min: number;
    sea_level?: number;
    grnd_level? : number;
    temp_kf? : number;
}

export interface sys {
    country: string;
    id: number;
    sunrise: number;
    sunset: number;
    type: number;
}

export interface weather {
    description: string;
    icon: string;
    id: number;
    main: string
}

export interface wind {
    deg: number;
    gust: number;
    speed: number;
}

export interface rain {
    "3h": number;
}
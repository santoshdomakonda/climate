import {main, weather, cloud, wind, rain, coordinates} from "../data-models/weather";

export interface forecast {
    cod: string;
    message: number;
    cnt: number;
    list: list[];
    city: city;
}

export interface list {
    dt: number;
    main: main;
    weather: weather[];
    clouds: cloud;
    wind: wind;
    visibility: number;
    pop: number;
    rain: rain;
    sys: sys;
    dt_txt: string;
}

export interface sys {
    pod: string;
}

export interface city {
    id: number;
    name: string;
    coord: coordinates;
    country: string;
    population: number;
    timezone: number;
    sunrise: number;
    sunset: number;
}

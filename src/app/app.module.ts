import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TempraturePipe } from './pipes/temprature.pipe';
import { TimePipe } from './pipes/time.pipe';
import { MainComponent } from './main/main.component';
import { DetailsComponent } from './details/details.component';
import { UnixDatePipe } from './pipes/unix-date.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TempraturePipe,
    TimePipe,
    MainComponent,
    DetailsComponent,
    UnixDatePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

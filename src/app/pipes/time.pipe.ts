import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'time'
})
export class TimePipe implements PipeTransform {

  transform(value: number): string {
    let timestamp  = moment.unix(value);
    return timestamp.format("hh:mm");
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'temprature'
})
export class TempraturePipe implements PipeTransform {

  transform(value: number, unit: string) {
    let tempareature: number
    if (value && !isNaN(value)) {
        if (unit === 'F') {
            tempareature = (value * 9 / 5) + 32;
            return tempareature.toFixed(2);
        }
        if (unit === 'C') {
            tempareature = (value - 32) * 5 / 9;
            return tempareature.toFixed(2);
        }
    }
    return;
}

}

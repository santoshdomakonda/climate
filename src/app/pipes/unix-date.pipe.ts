import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'unixDate'
})
export class UnixDatePipe implements PipeTransform {

  transform(value: number): string {
    let timestamp  = moment.unix(value);
    return timestamp.format("DD-MM-yyyy");
  }

}

import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { forecast } from '../data-models/fore-cast';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.less']
})
export class DetailsComponent implements OnInit {

  cast: forecast = <forecast>{};

  constructor(private appService: AppService) { }

  ngOnInit(): void {
    let city = localStorage.getItem('city') || 'London';
    this.appService.getForcaste(city).subscribe((response: forecast) => {
      this.cast = response;
      this.cast.list = this.cast.list.splice(0,5);
    });
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { forecast } from './data-models/fore-cast';
import { climate } from './data-models/weather';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private httpClient: HttpClient) { }

   public getCities() : Observable<climate[]> {
    let response1 = this.httpClient.get<climate>('https://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=3d8b309701a13f65b660fa2c64cdc517');
    let response2 = this.httpClient.get<climate>('https://api.openweathermap.org/data/2.5/weather?q=Bristol,uk&appid=3d8b309701a13f65b660fa2c64cdc517');
    let response3 = this.httpClient.get<climate>('https://api.openweathermap.org/data/2.5/weather?q=Cardiff,uk&appid=3d8b309701a13f65b660fa2c64cdc517');
    let response4 = this.httpClient.get<climate>('https://api.openweathermap.org/data/2.5/weather?q=Leeds,uk&appid=3d8b309701a13f65b660fa2c64cdc517');
    let response5 = this.httpClient.get<climate>('https://api.openweathermap.org/data/2.5/weather?q=Manchester,uk&appid=3d8b309701a13f65b660fa2c64cdc517');
    return forkJoin([response1, response2, response3, response4, response5]);
   }

   public getForcaste(city: string) : Observable<forecast> {
    return this.httpClient.get<forecast>("http://api.openweathermap.org/data/2.5/forecast?q="+ city + ",uk&appid=3d8b309701a13f65b660fa2c64cdc517")
    .pipe(map((response: forecast) => response));
   }
}
